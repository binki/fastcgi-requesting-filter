# Purpose

The way I’m used to fastcgi working is how Apache’s mod_fcgid does
it. Basically, to create an endpoint or application on your server,
just write a hybrid (fast)CGI script and place it in your webserver
where it should be accessed. Then configure a FastCGI module with CGI
fallback to run the scripts. There is no need to manually run a daemon
or configure your httpd with a “filter” daemon. Then you can deploy a
mix of FastCGI-capable scripts, CGI scripts, what-have-you, without
regard to the language/platform these scripts use. For example, you
might have:

index.cgi:

    #!/usr/bin/env php-cgi
    echo 'Hello, world!';

index.cgi:

    #!/usr/bin/env node
    // Only supports FastCGI for now, CGI support hopefully coming in the future:
    require('node-fastcgi').createServer(function (req, res) {
        res.end('Hello, world!');
    }).listen();

Or if you really want you can put perl or Python or even use a C
FastCGI library.

However, when I looked at trying to launch FastCGI with nginx, I found
that it *only* supports this filter mode. And most howtos about
running FastCGI anywhere are all about setting up PHP as a FastCGI
filter. Obviously, this would never work if I want to support CGI
scripts written in languages other than PHP. And there’s no point in
putting language-specific stuff in httpds anyway—it’s up to the script
to decide what language it wants to be implemented in.

In production you should really use apache and mod_fcgid instead. This
is only for when you’re stuck temporarily with something which doesn’t
implement FastCGI’s request mode.

# Installation

For compilation instructions, see [INSTALLING](INSTALLING.md).

# Configuration

To use FastCGI in Apache with mod_fcgid, you don’t need the program at
all. Apache mod_fcgid already does the right thing! However, you will
need to add the following configuration (the shown `Directory` is for
mod_userdir+suexec which would be configured in another part of your
httpd configuration. If you want to serve out of a vhost or put your
application in a system `/var/www/` directory, you will need to adjust
`<Directory/>` for your own purposes):

    <Directory /home/*/public_html>
        # Do not use AddHanlder because it matches files like blah.cgi.en (or blah.cgi.i_do_not_want_this_to_execute).
        <FilesMatch "\.cgi$">
            SetHandler fcgid-script
            Options +ExecCGI
        </FilesMatch>
    </Directory>
