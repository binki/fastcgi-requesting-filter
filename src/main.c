#include "config.h"

#include <stdio.h>
#include <unistd.h>

int main(int argc, char *const argv[])
{
    int optret;
    while (-1 != (optret = getopt(argc, argv, "hV")))
    {
        switch (optret)
        {
        case '?':
            fprintf(stderr, "Unrecognized option: %c\n", optopt);
            return 1;
        case 'h':
            printf("Usage: %s [-hV]\n", argv[0]);
            printf("\n");
            printf(" -h Show this help and exit.\n");
            printf(" -V Print version and exit.\n");
            return 0;
        case 'V':
            printf("%s\n", PACKAGE_STRING);
            return 0;
        }
    }
    return 0;
}
