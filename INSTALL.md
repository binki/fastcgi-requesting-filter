This uses autotools and supports
[MSYS2](https://msys2.github.io/)/w64. That means, on all platforms,
even Windows (after installing MSYS2), use the following to install:

    $ ./configure && make
    # make install

If you can, please create a custom package for your distribution and
let the package manager manage installing/uninstalling the files from
this package. We do not support any damage to your system caused by
installing without a package manager.
